import org.graalvm.compiler.nodeinfo.InputType;
import org.hexworks.zircon.api.*;
import org.hexworks.zircon.api.color.ANSITileColor;
import org.hexworks.zircon.api.data.Size;
import org.hexworks.zircon.api.data.Tile;
import org.hexworks.zircon.api.grid.TileGrid;
import org.hexworks.zircon.api.resource.TilesetResource;
import org.hexworks.zircon.api.uievent.KeyCode;
import org.hexworks.zircon.api.uievent.KeyboardEventType;
import org.hexworks.zircon.api.uievent.KeyCode;
import org.hexworks.zircon.api.uievent.Processed;

import org.hexworks.zircon.api.uievent.UIEventPhase;


import java.awt.*;

public class WorldDisplay {
    private static final TilesetResource TILESET = CP437TilesetResources.rexPaint16x16();
    static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    static double columns = screenSize.getWidth() / TILESET.getWidth();
    static double rows = screenSize.getHeight() / TILESET.getHeight();
    static Size terminalSize = Sizes.create((int) columns, (int) rows);

    private final static TileGrid tileGrid =  SwingApplications.startTileGrid(AppConfigs.newConfig()
            .withDefaultTileset(TILESET)
            .withSize(terminalSize)
            .withDebugMode(true)
            .withTitle("Game")
            .fullScreen()
            .build());

    public static TileGrid getTileGrid() {
        return tileGrid;
    }




    public static void display(Tile[][] map) {
         Tile[][] level = DungeonWallTest.dungeonMap();

TileGrid tileGrid= getTileGrid();

        tileGrid.onKeyboardEvent(KeyboardEventType.KEY_PRESSED, ((event, phase) -> {
            if (event.getKey().equals(KeyCode.ESCAPE)) {
                System.exit(0);
            }
            return UIEventResponses.processed();
        }));

        for (int i = 0; i < level.length; i++) {
            for (int j = 0; j < level.length; j++) {

                tileGrid.setTileAt(Positions.create(i, j), level[i][j]);

            }

        }
    }
}
