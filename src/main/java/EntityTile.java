import org.hexworks.zircon.api.TileColors;
import org.hexworks.zircon.api.Tiles;
import org.hexworks.zircon.api.data.Tile;

public class EntityTile {

    ///////////////////MAPS////////////////////MAPS//////////////////////

    static   final Tile FLOOR_CONCRETE = Tiles.newBuilder()
            .withCharacter('.')
            .withForegroundColor(TileColors.fromString("#c9c3c3"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static   final Tile FLOOR_GOLD = Tiles.newBuilder()
            .withCharacter('.')
            .withForegroundColor(TileColors.fromString("#ffce00"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static  final Tile WALLS_CONCRETE = Tiles.newBuilder()
            .withCharacter('#')
            .withForegroundColor(TileColors.fromString("#c9c3c3"))
            .withBackgroundColor(TileColors.fromString("#000000"))
            .build();

    static   final Tile DOORS_RIGHT_CONCRETE = Tiles.newBuilder()
            .withCharacter('+')
            .withForegroundColor(TileColors.fromString("#eda71c"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static  final Tile DOORS_LEFT_CONCRETE = Tiles.newBuilder()
            .withCharacter('+')
            .withForegroundColor(TileColors.fromString("#eda71c"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static  final Tile DOORS_UP_CONCRETE = Tiles.newBuilder()
            .withCharacter('+')
            .withForegroundColor(TileColors.fromString("#eda71c"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static   final Tile DOORS_DOWN_CONCRETE = Tiles.newBuilder()
            .withCharacter('+')
            .withForegroundColor(TileColors.fromString("#eda71c"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static  final Tile STAIRS_UP = Tiles.newBuilder()
            .withCharacter('>')
            .withForegroundColor(TileColors.fromString("#c9c3c3"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static   final Tile STAIRS_DOWN = Tiles.newBuilder()
            .withCharacter('<')
            .withForegroundColor(TileColors.fromString("#c9c3c3"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static   final Tile ROOM_CENTER_CONCRETE = Tiles.newBuilder()
            .withCharacter('.')
            .withForegroundColor(TileColors.fromString("#c9c3c3"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static final Tile TUNNEL_CONCRETE = Tiles.newBuilder()
            .withCharacter('?')
            .withForegroundColor(TileColors.fromString("#c9c3c3"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();

    static final Tile BORDER = Tiles.newBuilder()
            .withCharacter('~')
            .withForegroundColor(TileColors.fromString("#635e5e"))
            .withBackgroundColor(TileColors.fromString("#282727"))
            .build();


    static final Tile TUNNEL_TOP = Tiles.newBuilder()
            .withCharacter('.')
            .withForegroundColor(TileColors.transparent())
            .withBackgroundColor(TileColors.transparent())
            .build();

    static final Tile TUNNEL_BOT = Tiles.newBuilder()
            .withCharacter('.')
            .withForegroundColor(TileColors.transparent())
            .withBackgroundColor(TileColors.transparent())
            .build();

    static final Tile TUNNEL_LEFT = Tiles.newBuilder()
            .withCharacter('.')
            .withForegroundColor(TileColors.transparent())
            .withBackgroundColor(TileColors.transparent())
            .build();

    static final Tile TUNNEL_RIGHT = Tiles.newBuilder()
            .withCharacter('.')
            .withForegroundColor(TileColors.transparent())
            .withBackgroundColor(TileColors.transparent())
            .build();

    //LIVING///////////////////////////////////LIVING//////////////////////////


    static   final Tile PLAYER = Tiles.newBuilder()
            .withCharacter('@')
            .withForegroundColor(TileColors.fromString("#ffffff"))
            .withBackgroundColor(TileColors.transparent())
            .build();

}
