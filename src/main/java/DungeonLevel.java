import org.hexworks.zircon.api.data.Tile;

public class DungeonLevel {
/*
   1, create a 2d array of Tiles
   2. place random sized room and make sure they don't overlap
   3.  when the map is filled with rooms, place doors on every room
   4.connect doors and bury dead ends
   5.
   6.
   7.

 */

    Tile[][]level = new Tile[50][50];
    public static Tile[][] level(){
        //main method of the DungeonLevel class.
        // levelCreators compile all the other methods in this class and is called from other classes
        // to build the level map

        Tile [][] level =placeRooms();
        Tile [][] levelWithTunnels =  tunnelDig(level);
        Tile [][] levelWithFinishedTunnels = deleteTunnels(levelWithTunnels);
        boolean isGoodMap =false;



        return levelWithFinishedTunnels;

    }





    public static Tile[][] placeRooms() {

        Terrain terrain=new Terrain();


        //fills the level with wall tiles-in this case 0
        Tile[][] walls = new Tile[50][50];
        for (int i = 1; i < walls.length-1; i++) {
            for (int j = 1; j < walls[i].length-1; j++) {
              walls[i][j]= terrain.WALLS_CONCRETE().tile;
            }
        }


        int roomWidth= 9;
        int roomLength= 9;
        int startPositionX = randomNumber(3,walls.length - roomLength-2);
        int startPositionY = randomNumber(3,walls.length- roomWidth-3);
        int counter = 0;



        while (counter <= 10000 && roomLength > 4) {
            if (isValidPosition(startPositionX, startPositionY, roomLength, roomWidth, walls)) {

                boolean isPlaced = false;

                if (roomSpaceAvailable(walls, startPositionX, startPositionY, roomLength, roomWidth)) {
                    for (int i = startPositionX; i < (startPositionX + roomLength); i++) {
                        for (int j = startPositionY; j < (startPositionY + roomWidth); j++) {
                            walls[i][j] = terrain.FLOOR_CONCRETE().tile;
                            walls[startPositionX + roomLength / 2][startPositionY + roomWidth / 2] = terrain.ROOM_CENTER_CONCRETE().tile;
                            isPlaced= true;
                        }

                    }
                    int doorup=randomNumber(2,5);
                    int doordown=randomNumber(2,5);
                    int doorright=randomNumber(2,5);
                    int doorleft=randomNumber(2,5);
                    //to make sure that the left door is never at the end of the map
                    if(startPositionY + roomWidth /doorleft !=0){
                        walls[startPositionX - 1][startPositionY + roomWidth / doorleft] = terrain.DOORS_LEFT_CONCRETE().tile;
                    }else{
                        walls[startPositionX - 1][startPositionY + roomWidth / doorleft] = terrain.WALLS_CONCRETE().tile;

                    }
                    //right door
                    if((startPositionY + roomWidth / doorright) != walls.length-1 ){
                        walls[startPositionX + roomLength][startPositionY + roomWidth / doorright] = terrain.DOORS_RIGHT_CONCRETE().tile;

                    }else{
                        walls[startPositionX + roomLength][startPositionY + roomWidth / doorright] = terrain.WALLS_CONCRETE().tile;

                    }

                    if ((startPositionX + roomLength / doordown) != 0){
                        walls[startPositionX + roomLength / doordown][startPositionY + roomWidth] = terrain.DOORS_DOWN_CONCRETE().tile;

                    }else{
                        walls[startPositionX + roomLength / doordown][startPositionY + roomWidth] = terrain.WALLS_CONCRETE().tile;

                    }
                    if((startPositionX + roomLength / doorup)!=walls.length-1){
                        walls[startPositionX + roomLength / doorup][startPositionY - 1] = terrain.DOORS_UP_CONCRETE().tile;

                    }else{
                        walls[startPositionX + roomLength / doorup][startPositionY - 1] = terrain.WALLS_CONCRETE().tile;

                    }
                }

                if (roomSpaceAvailable(walls, startPositionX, startPositionY, roomLength,
                        roomWidth) == false) {
                    int randomN = randomNumber(1,11);
                    if(randomN>5){
                        startPositionX =2+ (int) (Math.random() * (walls.length - roomLength-2));

                    }else {
                        startPositionY = 2 + (int) (Math.random() * (walls.length - roomWidth - 2));
                    }
                }
                if(counter%3==0 && (roomLength >= 5)  && (roomWidth >=5) && isPlaced == true) {
                    roomLength --;
                    roomWidth --;
                }

            }

            counter++;
        }


        return walls;
    }
    public static boolean roomSpaceAvailable(Tile map1[][], int startPositionX, int startPositionY, int roomLength,
                                             int roomWidth) {
        // this method checks if there is enough free space

        Terrain terrain=new Terrain();


        for (int i = startPositionX-1; i <= (startPositionX + roomLength); i++) {
            for (int j = startPositionY-1; j <= (startPositionY + roomWidth); j++) {
                if (map1[i][j] == terrain.FLOOR_CONCRETE().tile) {
                    return false;
                }
            }
        }
        return true;
    }
    public static boolean isValidPosition(int startPositionX, int startPositionY, int roomLength, int roomWidth,
                                          Tile mapReplacement[][]) {

        // checks if the room is out of bounds

        if (2 > startPositionX || startPositionX + roomLength > mapReplacement.length - 2) {
            return false;
        }
        if (2 > startPositionY || startPositionY + roomWidth > mapReplacement[0].length - 2) {
            return false;
        }


        return true;

    }
   public static Tile[][] tunnelDig(Tile[][] map) {
// first find the doors of the rooms, then make tunnels in those directions until they hit a room while (map[row][j] == tile.wall() /* || (row) > 4 )
      int counter =0;
       int counter2 =0;
Terrain terrain = new Terrain();
       //places exit stairs in a random room center
      while(counter <1){
          int a= randomNumber(2,map.length);
          int b =randomNumber(2,map.length);
          if(map[a][b]==terrain.ROOM_CENTER_CONCRETE().tile){
              map[a][b]=terrain.DOORS_RIGHT_CONCRETE().tile;
              counter++;
          }else{
              a= randomNumber(2,map.length);
              b= randomNumber(2,map.length);
          }
      }
    /*   while(counter2 <1){
           int a= randomNumber(2,map.length);
           int b =randomNumber(2,map.length);
           if(map[a][b]==terrain.ROOM_CENTER_CONCRETE().tile){
               map[a][b]='@';
               counter2++;
           } else{
               a= randomNumber(2,map.length);
               b= randomNumber(2,map.length);
           }
       }*/
       while(counter <1){
           int a= randomNumber(2,map.length);
           int b =randomNumber(2,map.length);
           if(map[a][b]==terrain.ROOM_CENTER_CONCRETE().tile){
               map[a][b]=terrain.STAIRS_DOWN_CONCRETE().tile;
               counter++;
           }else{
               a= randomNumber(2,map.length);
               b= randomNumber(2,map.length);
           }
       }
       for(int i=0; i<map.length;i++) {
           for (int j = 0; j < map[i].length; j++) {
               if(i == 0 || i==map.length-1 || j==0 || j==map[i].length-1){
                   map[i][j]=terrain.BORDER().tile;
               }
           }
       }

       for (int x = 2; x < map.length - 2; x++) {
            for (int y = 1; y < map[0].length-2; y++) {
                if (map[x][y] == terrain.DOORS_UP_CONCRETE().tile) {


                    for (int a = y + 1; a < map[0].length - 3; a++) {
                        map[x][a] = terrain.TUNNEL_CONCRETE().tile;
                        if (map[x][a + 1] != terrain.WALLS_CONCRETE().tile) {
                            break;
                        }
                    }

                }
                if (map[x][y] == terrain.DOORS_LEFT_CONCRETE().tile) {


                    for (int a = x - 1; a > 3; a--) {
                        map[a][y] = terrain.TUNNEL_CONCRETE().tile;
                        if (map[a - 1][y] != terrain.WALLS_CONCRETE().tile) {
                            break;
                        }
                    }

                }
                if (map[x][y] == terrain.DOORS_RIGHT_CONCRETE().tile) {
                    for (int a = x + 1; a < map.length - 3; a++) {
                        map[a][y] = terrain.TUNNEL_CONCRETE().tile;
                        if (map[a + 1][y] != terrain.WALLS_CONCRETE().tile) {
                            break;
                        }
                    }


                }
                if (map[x][y] == terrain.DOORS_DOWN_CONCRETE().tile) {

                    for (int a = x - 1; a > 3; a--) {
                        map[x][a] = terrain.TUNNEL_CONCRETE().tile;
                        if (map[x][a - 1] != terrain.WALLS_CONCRETE().tile) {
                            break;
                        }
                    }

                }
            }

        }

        return map;
    }

    public static Tile[][] deleteTunnels(Tile[][] map) {

        Terrain terrain= new Terrain();

        for (int k = 0; k < 30; k++) {
            for (int i = 1; i < map.length - 1; i++) {
                for (int j = 1; j < map[i].length - 1; j++) {
                    int surroundingCounter = 0;

                    int surroundingCounter2 = 0;
                    int surroundingCounter4 = 0;

                    if (map[i][j] == terrain.TUNNEL_CONCRETE().tile) {
                        if (map[i + 1][j] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter++;
                        }
                        if (map[i - 1][j] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter++;
                        }
                        if (map[i][j + 1] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter++;
                        }
                        if (map[i][j - 1] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter++;
                        }

                    }
                    if (surroundingCounter > 2) {
                        map[i][j] = terrain.WALLS_CONCRETE().tile;
                        surroundingCounter = 0;
                    }

                    if (map[i][j] == terrain.TUNNEL_CONCRETE().tile) {
                        if (map[i + 1][j] == terrain.FLOOR_CONCRETE().tile) {
                            surroundingCounter2++;
                        }
                        if (map[i - 1][j] == terrain.FLOOR_CONCRETE().tile) {
                            surroundingCounter2++;
                        }
                        if (map[i][j + 1] == terrain.FLOOR_CONCRETE().tile) {
                            surroundingCounter2++;
                        }
                        if (map[i][j - 1] == terrain.FLOOR_CONCRETE().tile) {
                            surroundingCounter2++;
                        }

                    }
                    if (surroundingCounter2 > 1) {
                        map[i][j] = terrain.FLOOR_CONCRETE().tile;
                        surroundingCounter2 = 0;
                    }
                    int surroundingCounter3 = 0;

                    if (map[i][j] == terrain.DOORS_DOWN_CONCRETE().tile || map[i][j] == terrain.DOORS_UP_CONCRETE().tile
                            || map[i][j] == terrain.DOORS_LEFT_CONCRETE().tile|| map[i][j] == terrain.DOORS_RIGHT_CONCRETE().tile) {
                        if (map[i + 1][j] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter3++;
                        }
                        if (map[i - 1][j] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter3++;
                        }
                        if (map[i][j + 1] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter3++;
                        }
                        if (map[i][j - 1] == terrain.WALLS_CONCRETE().tile) {
                            surroundingCounter3++;
                        }

                    }
                    if (surroundingCounter3 > 2) {
                        map[i][j] = terrain.WALLS_CONCRETE().tile;
                        surroundingCounter3 = 0;
                    }

                }
            }

        }

        return map;
    }


    public static int randomNumber(int min, int max){
        int range = max-min;
        int randomN= (int) (Math.random()* range) + min;
        return randomN;
    }


}
