import org.hexworks.zircon.api.UIEventResponses;
import org.hexworks.zircon.api.data.Tile;
import org.hexworks.zircon.api.grid.TileGrid;
import org.hexworks.zircon.api.uievent.KeyCode;
import org.hexworks.zircon.api.uievent.KeyboardEventType;

public class GameMain {
    public static void main(String [] args){
        Tile[][] map = DungeonWallTest.dungeonMap();
        TileGrid tileGrid= WorldDisplay.getTileGrid();



        WorldDisplay.display(map);
    }
}
