import org.hexworks.zircon.api.data.Tile;

public class Terrain {
    boolean walkable;
    boolean digable;
    boolean spawnable;
    int durability;
    Tile tile;

    public Terrain FLOOR_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.FLOOR_CONCRETE;

        return this;
    }

    public Terrain WALLS_CONCRETE(){
        walkable =false;
        digable=false;
        spawnable=false;
        durability=100;
        tile= EntityTile.WALLS_CONCRETE;

        return this;
    }

    public Terrain TUNNEL_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.TUNNEL_CONCRETE;

        return this;
    }
    public Terrain TUNNEL_TOP(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.TUNNEL_TOP;

        return this;
    }

    public Terrain TUNNEL_DOWN(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.TUNNEL_BOT;

        return this;
    }
    public Terrain TUNNEL_LEFT(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.TUNNEL_LEFT;

        return this;
    }
    public Terrain TUNNEL_RIGHT(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.TUNNEL_RIGHT;

        return this;
    }

    public Terrain STAIRS_UP_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.STAIRS_UP;

        return this;
    }

    public Terrain STAIRS_DOWN_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.STAIRS_DOWN;

        return this;
    }
    public Terrain DOORS_UP_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.DOORS_UP_CONCRETE;

        return this;
    }

    public Terrain DOORS_DOWN_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.DOORS_DOWN_CONCRETE;

        return this;
    }

    public Terrain DOORS_RIGHT_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.DOORS_RIGHT_CONCRETE;

        return this;
    }

    public Terrain DOORS_LEFT_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.DOORS_LEFT_CONCRETE;

        return this;
    }

    public Terrain ROOM_CENTER_CONCRETE(){
        walkable =true;
        digable=false;
        spawnable=true;
        durability=100;
        tile= EntityTile.ROOM_CENTER_CONCRETE;

        return this;
    }
    public Terrain BORDER(){
        walkable =false;
        digable=false;
        spawnable=false;
        durability=100;
        tile= EntityTile.BORDER;

        return this;
    }

}
