import org.hexworks.zircon.api.data.Tile;

public class DungeonWallTest {

    public static Tile[][] dungeonMap(){
        Terrain terrain=new Terrain();

        Tile[][]dungeon = DungeonLevel.level();
        boolean isGoodMap=false;
        do {
            int wallCount = countWalls(dungeon);
            System.out.println(wallCount);
            if (wallCount > 1100){
                isGoodMap=true;
                break;
            }else {
                dungeon = DungeonLevel.level();
            }
        } while (!isGoodMap);
        return dungeon;
    }
    private static int countWalls(Tile[][] levelWithFinishedTunnels) {
        Terrain terrain=new Terrain();
        int wallCounter=0;
        for (int i = 0; i < levelWithFinishedTunnels.length; i++) {
            for (int j = 0; j < levelWithFinishedTunnels[i].length; j++) {
                if (levelWithFinishedTunnels[i][j] != terrain.WALLS_CONCRETE().tile) {
                    wallCounter++;
                }
            }
        }
        return wallCounter;
    }
}
